--
-- File generated with SQLiteStudio v3.4.4 on Tue Sep 26 07:52:57 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    category_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'Coffee'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'Dessert'
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name        TEXT (50) UNIQUE,
    product_price       DOUBLE    NOT NULL,
    product_size        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL,
    product_type        TEXT (5)  DEFAULT HCF
                                  NOT NULL,
    product_category    INTEGER   DEFAULT (1) 
                                  NOT NULL
                                  REFERENCES category (category_id) ON DELETE CASCADE
                                                                    ON UPDATE SET NULL
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        product_category
                    )
                    VALUES (
                        1,
                        'Espresso',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        product_category
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'SML',
                        '012',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        product_category
                    )
                    VALUES (
                        3,
                        'เค้กชิฟฟ่อนช็อกโกแลต',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        product_category
                    )
                    VALUES (
                        4,
                        'บัตเตอร์เค้ก',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    user_id       INTEGER   PRIMARY KEY,
    user_name     TEXT (50),
    user_gender   TEXT (3),
    user_password TEXT (50) NOT NULL,
    user_role               NOT NULL
);

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     1,
                     'user2',
                     'F',
                     'password',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     2,
                     'user1',
                     'M',
                     'password',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     3,
                     'wanwadee',
                     'M',
                     'password',
                     1
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     4,
                     'user3',
                     'F',
                     'password',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     5,
                     'user3',
                     'F',
                     'password',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     6,
                     'user3',
                     'M',
                     'password',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     7,
                     'user3',
                     'M',
                     'password',
                     2
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
